import { Component } from '@angular/core';
import { ViewController, NavParams } from 'ionic-angular';
import { CounterProvider } from '../../providers/counter/counter';

/**
 * Generated class for the CounterEditPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@Component({
  selector: 'page-counter-edit',
  templateUrl: 'counter-edit.html',
})
export class CounterEditPage {

  constructor(public viewController: ViewController, public navParams: NavParams, public counterProvider: CounterProvider) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CounterEditPage');
  }

  handleSubmit(form: any) {
    let options = this.navParams.data;
    if (options.action === 'add') return this.addCounter(form);
    if (options.action === 'edit') return this.editCounter(form);
  }

  addCounter(counterData) {
    this.counterProvider.addCounter(counterData)
      .then(() => {
        this.viewController.dismiss();
      });
  }

  editCounter(counterData) {
    this.viewController.dismiss();
  }

}
