import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { ListPage } from '../pages/list/list';
import { CounterEditPage } from '../pages/counter-edit/counter-edit';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { CounterComponent } from '../components/counter/counter';
import { CounterListComponent } from '../components/counter-list/counter-list';
import { CounterProvider } from '../providers/counter/counter';

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    ListPage,
    CounterEditPage,
    CounterComponent,
    CounterListComponent
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    ListPage,
    CounterEditPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    { provide: ErrorHandler, useClass: IonicErrorHandler },
    CounterProvider
  ]
})
export class AppModule { }
