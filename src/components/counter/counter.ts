import { Component, Input } from '@angular/core';
import { Counter } from './../../models/counter';

/**
 * Generated class for the CounterComponent component.
 *
 * See https://angular.io/docs/ts/latest/api/core/index/ComponentMetadata-class.html
 * for more info on Angular Components.
 */
@Component({
  selector: 'counter',
  templateUrl: 'counter.html'
})
export class CounterComponent {

  @Input() counter: Counter;

  constructor() {
    console.log('Hello CounterComponent Component');
  }

}
