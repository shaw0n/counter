import { Component } from '@angular/core';
import { Counter } from '../../models/counter';
import { ModalController } from 'ionic-angular';
import { CounterEditPage } from './../../pages/counter-edit/counter-edit';
import { CounterProvider } from './../../providers/counter/counter';

/**
 * Generated class for the CounterListComponent component.
 *
 * See https://angular.io/docs/ts/latest/api/core/index/ComponentMetadata-class.html
 * for more info on Angular Components.
 */
@Component({
  selector: 'counter-list',
  templateUrl: 'counter-list.html'
})
export class CounterListComponent {

  counters: Counter[];

  constructor(public modalController: ModalController, public counterProvider: CounterProvider) {
    counterProvider.getCounters()
      .then((counters) => {
        this.counters = counters;
      })
  }

  addCounter() {
    let modalOptions = {
      action: 'add'
    };

    let addCounterModal = this.modalController.create(CounterEditPage, modalOptions);
    addCounterModal.present();
  }

}
