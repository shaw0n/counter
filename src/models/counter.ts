export interface Counter {
  title: string;
  value: number;
}
