import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';

import { Counter } from '../../models/counter';

/*
  Generated class for the CounterProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular DI.
*/
@Injectable()
export class CounterProvider {

  counters: Counter[];

  constructor() {
    this.counters = [
      {
        title: 'Counter 1',
        value: 2
      },
      {
        title: 'Counter 2',
        value: 3
      }
    ];
  }

  getCounters(): Promise<Counter[]> {
    return new Promise((resolve, reject) => {
      resolve(this.counters);
    })
  }

  addCounter(counter: Counter): Promise<any> {
    return new Promise((resolve, reject) => {
      this.counters.push(counter);
      resolve();
    })
  }

  editCounter(counter: Counter, index): Promise<any> {
    return new Promise((resolve, reject) => {
      this.counters[index] = counter;
      resolve();
    })
  }

}
